<!--partie code traitement avec BDD-->
<div class="container">
         <div class="row">
      <?php

                    //connection à la BDD
                    include 'infoconnexion.inc.php';
                    $mysqli = new mysqli($a,$b,$c,$d);
                    if ($mysqli->connect_errno) {
                    echo "Echec lors de la connexion à MySQL : (" . $mysqli->
                    connect_errno . ") " . $mysqli->connect_error;
                    }else{
                     $res=$mysqli->query("SELECT * FROM poster");
                     if(!$res){
                     die('Echec de la requête SQL :'.$mysqli->error);
                     }
                     else{
                       while($tuple=$res->fetch_assoc())
                       {
                         //$chemin=htmlentities($tuple['image']);
                          ?>


                          <div class="col-md-4">
                            <br>

                                <div class="card" style="width: 20rem; height:40rem">
                                  <img src="<?php  echo htmlentities($tuple['chemin']) ?>" class="card-img-top" alt="...">
                                    <div class="card-body">
                                      <h5 class="card-title"><?php echo '<p>'.htmlentities($tuple['nomPoster']).'</p>' ?></h5>
                                      <h6 class="card-text "><?php echo htmlentities($tuple['descriptif']) ?></h6>
                                      <p class="card-text font-italic">Fait par: <?php echo htmlentities($tuple['auteur']) ?></p>

                                    </div>
                                 </div>
                              </div>

<?php

}}}

      ?>
    </div>
 </div>
