<!DOCTYPE html>
<html lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>
      PING
    </title>
     <link rel="stylesheet" href="css/bootstrap.min.css">
  </head>

    <img src="esigelec.jpg" alt="..." class="img-responsive" width="100%" height="250" >
  <body background= "background.png" style="background-repeat: no-repeat;background-size:100% 100%;background-attachment:fixed;">
    <div class="container_fluid background.png">

    </div>

              <nav class="navbar navbar-expand-md navbar-expand-sm navbar-dark bg-dark" >
            <a class="navbar-brand font-weight-bold" href="#">Bienvenue au PING</a>
              <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                  <span class="navbar-toggler-icon"></span>
                  </button>
              <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                  <li class="nav-item active">
                    <a class="nav-link" href="pageAccueil.php">Accueil <span class="sr-only">(current)</span></a>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link" href="connexion.php">Connexion</a>
                  </li>
                  <li class="nav-item active">
                    <a class="nav-link" href="pageInscription.php">Inscription</a>
                  </li>
                </ul>
                <form class="form-inline my-2 my-lg-0">
                  <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                  <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
              </div>
            </nav>
            <div class="alert alert-success" role="alert">
  Inscription réussie!
</div>
              <div class="container-fluid">
                <div class="row">
                  <div class="col-md-6">

            <div id="carouselExampleSlidesOnly" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="ping62.jpg" class="d-block w-60" alt="" width="500" height="350">
    </div>
    <div class="carousel-item">
      <img src="ping3.jpg"class=" d-block w-60"  alt="" width="500" height="350">
    </div>
    <div class="carousel-item ">
      <img src="vote.jpg"  class="d-block w-60" alt="" width="500" height="350">
    </div>
  </div>
</div>
<h1>

</h1>
  </div>
  <article class="col-md-6">
    <div class="pull-right">
    <h1 class=" text-light font-weight-bold">QU'EST CE QUE LE PING? </h1>
<p class="text-light">Le Projet Ingénieur(PING)consiste à conduire un projet intégrant l’ensemble des dimensions technologiques, organisationnelles, financières et humaines. Il est mené en équipe de 6 étudiants et s’étend sur la 2e et 3e année du cycle ingénieur.
Les projets sont proposés principalement par des entreprises. Ils peuvent être liés à des projets de recherche ou être à l’initiative des étudiants s’il s’agit d’une création d’entreprise.
Chaque équipe a pour mission de rechercher le sujet auprès d’entreprises ou de structures de recherche et de développement,de réaliser une étude de faisabilité technico économique validée par la structure donneuse d’ordre,d'élaborer le cahier des charges,de réaliser le projet,d'exposer publiquement le résultat de ses travaux.
Chaque groupe est suivi par un binôme d’encadrement et un instructeur. Il peut avoir recours à des experts pointus sur des points précis du projet.</p>


        </div>

    </div>
  </div>

  <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
